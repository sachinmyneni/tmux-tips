# Tmux Tips



## Getting started

Copy the relavent portion of the .bashrc or .tcshrc and any supporting scripts to your home directory

## How do I know which shell I am using?

1. If your machine allows it, try _ypcat <your username>_ and look at the last part of the line. 
1. Another hack-y way of finding your shell is to run _basename $0_. This command will fail for TCSH and print out the first character of your shell's name. 't' or give the full shellname for BASH.

## What to do
1. If you are using BASH, copy the contents of the .bash_profile file into your own .bash_profile. 
2. If you are using TCSH, copy the contents of the .tcshrc into your own .tcshrc. It can be anywhere but I like to add new things to the bottom. Then copy the tm.tcsh script into your ~/bin directory. If you place it anywhere else, please update the alias in the .tcshrc.

## Problems, Questions, RFEs
1. Go to the **issues** on the tab and create a new issue and I will try to work on it as time permits.
