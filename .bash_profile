# Once sourced can be called as tm ls, tm n <session name> etc
tm() {
    case "$1" in
        ls) tmux list-sessions "${@:2}" ;;
        n) tmux new-session -s "${@:2}" ;;
        a) tmux attach -t "${@:2}" ;;
        *) tmux "$@" ;;
    esac
}
