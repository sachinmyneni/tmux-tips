
#!/bin/tcsh

switch ($1)
    case 'ls':
        tmux list-sessions $argv[2-]
        breaksw
    case 'n':
        tmux new-session -s $argv[2-]
        breaksw
    case 'a':
        tmux attach -t $argv[2-]
        breaksw
    default:
        tmux $argv:q
        break
endsw
